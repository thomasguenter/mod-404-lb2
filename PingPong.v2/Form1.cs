﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong.v2
{
    public partial class Form1 : Form
    {
        private int directionX = 8;
        private int directionY = 4;
        private int points = 0;
        
        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }
        
        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
            // Ball bewegen
            picBall.Location = new Point(picBall.Location.X + directionX, picBall.Location.Y + directionY);
            
            // Ball trifft rechte Wand
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width && picBall.Location.Y + picBall.Height >= picSchlägerRechts.Location.Y && picBall.Location.Y <= picSchlägerRechts.Location.Y + picSchlägerRechts.Height)
            {
                // Zähle 10 Punkte dazu
                directionX = -directionX;
                points += 10;
            }
            
            
            // Ball trifft linke Wand
            if (picBall.Location.X <= 0)
            {
                directionX = -directionX;
            }
            
            // Ball trifft obere Wand
            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                directionY = -directionY;
            }
            
            // Ball trifft untere Wand
            if (picBall.Location.Y < 0)
            {
                directionY = -directionY;
            }
            
            // Punktzahl anzeigen
            txtPunkte.Text = Convert.ToString(points);
            
        }

        private void vsbSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X, vsbSchlägerRechts.Value);
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            {
                // Schläger an rechte Wand
                picSchlägerRechts.Location = new Point(pnlSpiel.Width - picSchlägerRechts.Width, pnlSpiel.Height / 2);
            
                // Bildlaufliste neben Panel, max./min. und aktuellen Wert
                vsbSchlägerRechts.Height = pnlSpiel.Height;
                vsbSchlägerRechts.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
                vsbSchlägerRechts.Minimum = 0;
                vsbSchlägerRechts.Maximum = pnlSpiel.Height - picSchlägerRechts.Height + vsbSchlägerRechts.LargeChange;
                vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
            }
        }
    }
}