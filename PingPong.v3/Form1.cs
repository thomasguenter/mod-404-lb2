﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong.v3
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        private int _points = 0;
        public Form1()
        {
            InitializeComponent();
        }
        
        // Spiel starten
        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }
        
        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
            // Ball bewegen
            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);
            
            // Ball triff rechte Wand
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width && picBall.Location.Y + picBall.Height >= picSchlägerRechts.Location.Y && picBall.Location.Y <= picSchlägerRechts.Location.Y + picSchlägerRechts.Height)
            {
                // Zähle 10 Punkte dazu
                _directionX = -_directionX;
                _points += 10;
            }
        
            
            // Ball trifft linke Wand
            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
            }
            
            // Ball trifft obere Wand
            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }
            
            // Punkte anzeigen
            txtPunkte.Text = Convert.ToString(_points);
        }

        
        // Überprüfen ob h,v,p,s gedrückt wurde
        
        
        // Wird benötigt, damit die Pfeiltasten nicht von Element zu Element springen
        // Kontrolliere den Ball mit den Pfeiltasten
        protected override bool ProcessDialogKey(Keys keyData)
        {

            if (keyData == Keys.Up)
            {
                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
                return true;    // verhindert das der Fokus weitergegeben wird
            }

            else if (keyData == Keys.Down)
            {
                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
                return true;
            }
            
            else if (keyData == Keys.Left)
            {
                picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
                return true;
            }
            
            else if (keyData == Keys.Right)
            {
                picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
        private void btnHoch_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
        }
        private void btnLinks_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
        }
        private void btnRechts_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
        }
        private void btnTief_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + 0, picBall.Location.Y + 25);
        }

        // Schläger bewegen mit Bildlaufleiste
        private void vsbSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X, vsbSchlägerRechts.Value);
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Schläger an rechte Wand
            picSchlägerRechts.Location = new Point(pnlSpiel.Width - picSchlägerRechts.Width - picSchlägerRechts.Width, pnlSpiel.Height / 2);
            
            // Bildlaufliste neben Panel, max/min und aktueller Wert
            vsbSchlägerRechts.Height = pnlSpiel.Height;
            vsbSchlägerRechts.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
            vsbSchlägerRechts.Minimum = 0;
            vsbSchlägerRechts.Maximum = pnlSpiel.Height - picSchlägerRechts.Height + vsbSchlägerRechts.LargeChange;
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }
    }
}