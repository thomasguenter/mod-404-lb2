﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StandardEvents_AB03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            txtAusgabe.Text += "Klick" + Environment.NewLine;
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            txtAusgabe.Text += "Doppelklick" + Environment.NewLine;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    txtAusgabe.Text += "Left Mousebutton down" + Environment.NewLine;
                    break;
                case MouseButtons.Right:
                    txtAusgabe.Text += "Right Mousebutton down" + Environment.NewLine;
                    break;
                case MouseButtons.Middle:
                    txtAusgabe.Text += "Middle Mousebutton down" + Environment.NewLine;
                    break;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    txtAusgabe.Text += "Left Mousebutton up" + Environment.NewLine;
                    break;
                case MouseButtons.Right:
                    txtAusgabe.Text += "Right Mousebutton up" + Environment.NewLine;
                    break;
                case MouseButtons.Middle:
                    txtAusgabe.Text += "Middle Mousebutton up" + Environment.NewLine;
                    break;
            }
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            txtAusgabe.Text += "Key down" + Environment.NewLine;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            txtAusgabe.Text += "Key up" + Environment.NewLine;
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAusgabe.Text += e.KeyChar + " - key pressed" + Environment.NewLine;
        }
    }
}